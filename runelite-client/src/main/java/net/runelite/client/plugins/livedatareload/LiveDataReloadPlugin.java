package net.runelite.client.plugins.livedatareload;

import net.runelite.client.plugins.Plugin;
import net.runelite.client.plugins.PluginDescriptor;

@PluginDescriptor(name = "Live Data Reload", hidden = true)
public final class LiveDataReloadPlugin extends Plugin {

    private LiveDataReloadThread liveDataReloadThread;

    @Override
    protected void startUp() throws Exception {
        liveDataReloadThread = injector.getInstance(LiveDataReloadThread.class);
        liveDataReloadThread.start();
    }

    @Override
    protected void shutDown() {
        if (liveDataReloadThread != null) {
            liveDataReloadThread.stopWatching();
        }
    }

}
