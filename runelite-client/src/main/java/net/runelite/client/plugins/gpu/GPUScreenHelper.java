package net.runelite.client.plugins.gpu;

import java.awt.*;

/**
 * @author Jire
 */
public enum GPUScreenHelper {
    ;

    public static int getHighestScreenRefreshRate() {
        int highestRefreshRate = 60;
        GraphicsEnvironment localGraphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
        if (localGraphicsEnvironment.isHeadlessInstance()) {
            return highestRefreshRate;
        }
        for (GraphicsDevice screenDevice : localGraphicsEnvironment.getScreenDevices()) {
            int refreshRate = screenDevice.getDisplayMode().getRefreshRate();
            if (refreshRate > highestRefreshRate) {
                highestRefreshRate = refreshRate;
            }
        }
        return highestRefreshRate;
    }

    public static final int HIGHEST_SCREEN_REFRESH_RATE = getHighestScreenRefreshRate();

}
