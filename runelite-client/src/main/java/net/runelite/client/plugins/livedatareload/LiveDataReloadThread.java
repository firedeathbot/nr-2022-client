package net.runelite.client.plugins.livedatareload;

import com.google.inject.Inject;
import com.sun.nio.file.SensitivityWatchEventModifier;
import net.runelite.api.Client;
import net.runelite.rs.api.RSClient;
import net.runelite.rs.api.RSEvictingDualNodeHashTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.*;

import static java.nio.file.StandardWatchEventKinds.*;

final class LiveDataReloadThread extends Thread {

    private static final Logger logger = LoggerFactory.getLogger(LiveDataReloadThread.class);

    private final Path liveReloadScriptsPath;

    private final RSClient client;

    private final WatchService watchService;

    private volatile boolean stopped;

    @Inject
    private LiveDataReloadThread(Client client) throws IOException {
        setName("Watch Thread");
        setDaemon(true);
        setPriority(Thread.MIN_PRIORITY);

        this.client = (RSClient) client;

        liveReloadScriptsPath = Path.of(System.getProperty("user.home"), ".osnr", "live_archives");
        if (!Files.exists(liveReloadScriptsPath)) {
            stopped = true;
            watchService = null;
        } else {
            watchService = FileSystems.getDefault().newWatchService();
            liveReloadScriptsPath.register(watchService,
                    new WatchEvent.Kind[]{ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY},
                    SensitivityWatchEventModifier.HIGH);
        }
    }

    @Override
    public void run() {
        try {
            logger.info("Starting live data reload service...");

            while (!stopped && !Thread.interrupted()) {
                final WatchKey key;
                if ((key = watchService.take()) == null) break;
                for (WatchEvent<?> event : key.pollEvents()) {
                    final WatchEvent.Kind<?> kind = event.kind();
                    if (kind == OVERFLOW) continue;

                    final Path path = (Path) event.context();
                    logger.info("Live data changed: {}", path);

                    try {
                        if (kind == ENTRY_MODIFY) {
                            String[] split = path.toString()
                                    .replace(".bin", "")
                                    .split("-");
                            int index = Integer.parseInt(split[0]);
                            int archiveId = Integer.parseInt(split[1]);
                            int fileId = Integer.parseInt(split[2]);

                            // scripts index
                            if (index == 12) {
                                Object[] files = client.getIndexScripts().getFiles()[archiveId];
                                if (files != null) {
                                    files[fileId] = null;
                                }
                                RSEvictingDualNodeHashTable cache = client.getScriptCache();
                                cache.remove(archiveId);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (!key.reset()) {
                    stopWatching();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            stopWatching();
        }
    }

    void stopWatching() {
        logger.info("Stopping live data reload service...");

        stopped = true;
    }

}
