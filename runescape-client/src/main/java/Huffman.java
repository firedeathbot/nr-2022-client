import net.runelite.mapping.Export;
import net.runelite.mapping.Implements;
import net.runelite.mapping.ObfuscatedName;
import net.runelite.mapping.ObfuscatedSignature;

@ObfuscatedName("jx")
@Implements("Huffman")
public class Huffman {
	@ObfuscatedName("o")
	@Export("masks")
	int[] masks;
	@ObfuscatedName("q")
	@Export("bits")
	byte[] bits;
	@ObfuscatedName("l")
	@Export("keys")
	int[] keys;

	public Huffman(byte[] var1) {
		int var2 = var1.length; // L: 9
		this.masks = new int[var2]; // L: 10
		this.bits = var1; // L: 11
		int[] var3 = new int[33]; // L: 12
		this.keys = new int[8]; // L: 13
		int var4 = 0; // L: 14

		for (int var5 = 0; var5 < var2; ++var5) { // L: 15
			byte var6 = var1[var5]; // L: 16
			if (var6 != 0) { // L: 17
				int var7 = 1 << 32 - var6; // L: 18
				int var8 = var3[var6]; // L: 19
				this.masks[var5] = var8; // L: 20
				int var9;
				int var10;
				int var11;
				int var12;
				if ((var8 & var7) != 0) { // L: 22
					var9 = var3[var6 - 1];
				} else {
					var9 = var8 | var7; // L: 24

					for (var10 = var6 - 1; var10 >= 1; --var10) { // L: 25
						var11 = var3[var10]; // L: 26
						if (var11 != var8) { // L: 27
							break;
						}

						var12 = 1 << 32 - var10; // L: 28
						if ((var11 & var12) != 0) { // L: 29
							var3[var10] = var3[var10 - 1]; // L: 30
							break;
						}

						var3[var10] = var11 | var12; // L: 33
					}
				}

				var3[var6] = var9; // L: 38

				for (var10 = var6 + 1; var10 <= 32; ++var10) { // L: 39
					if (var8 == var3[var10]) { // L: 40
						var3[var10] = var9;
					}
				}

				var10 = 0; // L: 42

				for (var11 = 0; var11 < var6; ++var11) { // L: 43
					var12 = Integer.MIN_VALUE >>> var11; // L: 44
					if ((var8 & var12) != 0) { // L: 45
						if (this.keys[var10] == 0) { // L: 46
							this.keys[var10] = var4;
						}

						var10 = this.keys[var10]; // L: 47
					} else {
						++var10; // L: 49
					}

					if (var10 >= this.keys.length) { // L: 50
						int[] var13 = new int[this.keys.length * 2]; // L: 51

						for (int var14 = 0; var14 < this.keys.length; ++var14) { // L: 52
							var13[var14] = this.keys[var14];
						}

						this.keys = var13; // L: 53
					}

					var12 >>>= 1; // L: 55
				}

				this.keys[var10] = ~var5; // L: 57
				if (var10 >= var4) { // L: 58
					var4 = var10 + 1;
				}
			}
		}

	} // L: 60

	@ObfuscatedName("o")
	@ObfuscatedSignature(
		descriptor = "([BII[BII)I",
		garbageValue = "-1517056524"
	)
	@Export("compress")
	int compress(byte[] source, int sourceOffset, int length, byte[] dest, int destOffset) {
		int key = 0; // L: 63
		int pos = destOffset << 3; // L: 64

		for (length += sourceOffset; sourceOffset < length; ++sourceOffset) { // L: 65 66
			int _char = source[sourceOffset] & 255; // L: 67
			int encoding = this.masks[_char]; // L: 68
			byte frequency = this.bits[_char]; // L: 69
			if (frequency == 0) { // L: 70
				throw new RuntimeException("" + _char);
			}

			int bytePos = pos >> 3; // L: 71
			int remainder = pos & 7; // L: 72
			key &= -remainder >> 31; // L: 73
			int comparedBytePos = (remainder + frequency - 1 >> 3) + bytePos; // L: 74
			remainder += 24; // L: 76
			dest[bytePos] = (byte)(key |= encoding >>> remainder); // L: 77
			if (bytePos < comparedBytePos) { // L: 78
				++bytePos; // L: 79
				remainder -= 8; // L: 80
				dest[bytePos] = (byte)(key = encoding >>> remainder); // L: 81
				if (bytePos < comparedBytePos) { // L: 82
					++bytePos; // L: 83
					remainder -= 8; // L: 84
					dest[bytePos] = (byte)(key = encoding >>> remainder); // L: 85
					if (bytePos < comparedBytePos) { // L: 86
						++bytePos; // L: 87
						remainder -= 8; // L: 88
						dest[bytePos] = (byte)(key = encoding >>> remainder); // L: 89
						if (bytePos < comparedBytePos) { // L: 90
							++bytePos; // L: 91
							remainder -= 8; // L: 92
							dest[bytePos] = (byte)(key = encoding << -remainder); // L: 93
						}
					}
				}
			}

			pos += frequency; // L: 95
		}

		return (pos + 7 >> 3) - destOffset; // L: 97
	}

	@ObfuscatedName("q")
	@ObfuscatedSignature(
		descriptor = "([BI[BIII)I",
		garbageValue = "-1136816719"
	)
	@Export("decompress")
	int decompress(byte[] source, int sourceOffset, byte[] dest, int destOffset, int length) {
		if (length == 0) { // L: 101
			return 0;
		} else {
			int keyIndex = 0; // L: 102
			length += destOffset; // L: 103
			int i = sourceOffset;

			while (true) {
				byte _char = source[i]; // L: 106
				if (_char < 0) { // L: 108
					keyIndex = this.keys[keyIndex];
				} else {
					++keyIndex; // L: 109
				}

				int key;
				if ((key = this.keys[keyIndex]) < 0) { // L: 110
					dest[destOffset++] = (byte)(~key); // L: 111
					if (destOffset >= length) { // L: 112
						break;
					}

					keyIndex = 0; // L: 113
				}

				if ((_char & 64) != 0) { // L: 115
					keyIndex = this.keys[keyIndex];
				} else {
					++keyIndex; // L: 116
				}

				if ((key = this.keys[keyIndex]) < 0) { // L: 117
					dest[destOffset++] = (byte)(~key); // L: 118
					if (destOffset >= length) { // L: 119
						break;
					}

					keyIndex = 0; // L: 120
				}

				if ((_char & 32) != 0) { // L: 122
					keyIndex = this.keys[keyIndex];
				} else {
					++keyIndex; // L: 123
				}

				if ((key = this.keys[keyIndex]) < 0) { // L: 124
					dest[destOffset++] = (byte)(~key); // L: 125
					if (destOffset >= length) { // L: 126
						break;
					}

					keyIndex = 0; // L: 127
				}

				if ((_char & 16) != 0) { // L: 129
					keyIndex = this.keys[keyIndex];
				} else {
					++keyIndex; // L: 130
				}

				if ((key = this.keys[keyIndex]) < 0) { // L: 131
					dest[destOffset++] = (byte)(~key); // L: 132
					if (destOffset >= length) { // L: 133
						break;
					}

					keyIndex = 0; // L: 134
				}

				if ((_char & 8) != 0) { // L: 136
					keyIndex = this.keys[keyIndex];
				} else {
					++keyIndex; // L: 137
				}

				if ((key = this.keys[keyIndex]) < 0) { // L: 138
					dest[destOffset++] = (byte)(~key); // L: 139
					if (destOffset >= length) { // L: 140
						break;
					}

					keyIndex = 0; // L: 141
				}

				if ((_char & 4) != 0) { // L: 143
					keyIndex = this.keys[keyIndex];
				} else {
					++keyIndex; // L: 144
				}

				if ((key = this.keys[keyIndex]) < 0) { // L: 145
					dest[destOffset++] = (byte)(~key); // L: 146
					if (destOffset >= length) { // L: 147
						break;
					}

					keyIndex = 0; // L: 148
				}

				if ((_char & 2) != 0) { // L: 150
					keyIndex = this.keys[keyIndex];
				} else {
					++keyIndex; // L: 151
				}

				if ((key = this.keys[keyIndex]) < 0) { // L: 152
					dest[destOffset++] = (byte)(~key); // L: 153
					if (destOffset >= length) { // L: 154
						break;
					}

					keyIndex = 0; // L: 155
				}

				if ((_char & 1) != 0) {
					keyIndex = this.keys[keyIndex]; // L: 157
				} else {
					++keyIndex; // L: 158
				}

				if ((key = this.keys[keyIndex]) < 0) { // L: 159
					dest[destOffset++] = (byte)(~key); // L: 160
					if (destOffset >= length) { // L: 161
						break;
					}

					keyIndex = 0; // L: 162
				}

				++i; // L: 105
			}

			return i + 1 - sourceOffset; // L: 165
		}
	}
}
