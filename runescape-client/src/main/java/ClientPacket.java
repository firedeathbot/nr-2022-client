import net.runelite.mapping.Export;
import net.runelite.mapping.Implements;
import net.runelite.mapping.ObfuscatedGetter;
import net.runelite.mapping.ObfuscatedName;
import net.runelite.mapping.ObfuscatedSignature;

@ObfuscatedName("jj")
@Implements("ClientPacket")
public class ClientPacket implements class261 {
	@ObfuscatedName("o")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket CLANCHANNEL_KICKUSER;
	@ObfuscatedName("q")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket EVENT_KEYBOARD;
	@ObfuscatedName("l")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket OPLOC6;
	@ObfuscatedName("k")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	static final ClientPacket UNUSED1;
	@ObfuscatedName("a")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket EVENT_MOUSE_MOVE;
	@ObfuscatedName("m")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket LOGIN_STATISTICS;
	@ObfuscatedName("p")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket UNKNOWN1;
	@ObfuscatedName("s")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket EVENT_APPLET_FOCUS;
	@ObfuscatedName("r")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket IF_BUTTON4;
	@ObfuscatedName("v")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket MOVE_GAMECLICK;
	@ObfuscatedName("y")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket AFFINEDCLANSETTINGS_ADDBANNED_FROMCHANNEL;
	@ObfuscatedName("c")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket IGNORELIST_ADD;
	@ObfuscatedName("w")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket IF_BUTTON5;
	@ObfuscatedName("b")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket IF1_BUTTON4;
	@ObfuscatedName("t")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket OPPLAYER4;
	@ObfuscatedName("g")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket IF_BUTTONT;
	@ObfuscatedName("x")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket RESUME_P_NAMEDIALOG;
	@ObfuscatedName("n")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket IF_BUTTON2;
	@ObfuscatedName("e")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket TELEPORT;
	@ObfuscatedName("h")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	static final ClientPacket UNUSED_2;
	@ObfuscatedName("f")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket IF_BUTTON6;
	@ObfuscatedName("d")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	static final ClientPacket UNUSED_3;
	@ObfuscatedName("j")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket CLAN_KICKUSER;
	@ObfuscatedName("z")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket OPHELD1;
	@ObfuscatedName("i")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket RESUME_P_COUNTDIALOG;
	@ObfuscatedName("u")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket MESSAGE_PUBLIC;
	@ObfuscatedName("ag")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket BUG_REPORT;
	@ObfuscatedName("ar")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket OPPLAYER2;
	@ObfuscatedName("am")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket OPPLAYER6;
	@ObfuscatedName("ac")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket OPNPC4;
	@ObfuscatedName("ab")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket OPOBJ5;
	@ObfuscatedName("aj")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket OPHELD5;
	@ObfuscatedName("ae")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket OPPLAYER7;
	@ObfuscatedName("az")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket OPLOCU;
	@ObfuscatedName("ap")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket NO_TIMEOUT;
	@ObfuscatedName("as")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket OPPLAYER8;
	@ObfuscatedName("au")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket OPHELD3;
	@ObfuscatedName("ak")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket OPNPCU;
	@ObfuscatedName("af")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket OPLOC1;
	@ObfuscatedName("al")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket OPNPCT;
	@ObfuscatedName("aq")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket CLIENT_CHEAT;
	@ObfuscatedName("ad")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket IF_BUTTON8;
	@ObfuscatedName("an")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket EXIT_FREECAM;
	@ObfuscatedName("aw")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket FRIENDLIST_DEL;
	@ObfuscatedName("ah")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket RESUME_PAUSEBUTTON;
	@ObfuscatedName("ao")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket AFFINEDCLANSETTINGS_SETMUTED_FROMCHANNEL;
	@ObfuscatedName("av")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket OPLOC5;
	@ObfuscatedName("ai")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket OPPLAYER5;
	@ObfuscatedName("ay")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket IF1_BUTTON3;
	@ObfuscatedName("aa")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket OPOBJ3;
	@ObfuscatedName("ax")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket FRIENDLIST_ADD;
	@ObfuscatedName("at")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket OPLOC4;
	@ObfuscatedName("br")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket OPPLAYERU;
	@ObfuscatedName("ba")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket CLOSE_MODAL;
	@ObfuscatedName("bk")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket IF_BUTTON7;
	@ObfuscatedName("bi")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket DETECT_MODIFIED_CLIENT;
	@ObfuscatedName("bc")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket OPNPC1;
	@ObfuscatedName("bo")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	static final ClientPacket UNUSED5;
	@ObfuscatedName("bl")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket IF_BUTTON1;
	@ObfuscatedName("bs")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket IF1_BUTTON1;
	@ObfuscatedName("bx")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket IF1_BUTTON2;
	@ObfuscatedName("bd")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket OPOBJT;
	@ObfuscatedName("bj")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket OPOBJU;
	@ObfuscatedName("be")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket IF_BUTTON3;
	@ObfuscatedName("bf")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket UPDATE_APPEARANCE;
	@ObfuscatedName("bz")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket OPOBJ1;
	@ObfuscatedName("bv")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket BUTTON_CLICK;
	@ObfuscatedName("bt")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket IF_BUTTON9;
	@ObfuscatedName("bu")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket CLAN_JOINCHAT_LEAVECHAT;
	@ObfuscatedName("bq")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket OPNPC6;
	@ObfuscatedName("bh")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket OPOBJ4;
	@ObfuscatedName("bb")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket OPHELDT;
	@ObfuscatedName("bm")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket RESUME_P_OBJDIALOG;
	@ObfuscatedName("by")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket OPOBJ2;
	@ObfuscatedName("bn")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket OPPLAYERT;
	@ObfuscatedName("bg")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket MOVE_MINIMAPCLICK;
	@ObfuscatedName("bp")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket OPLOCT;
	@ObfuscatedName("bw")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket IF1_BUTTON5;
	@ObfuscatedName("co")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket FRIEND_SETRANK;
	@ObfuscatedName("ck")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket IDLE_LOGOUT;
	@ObfuscatedName("cr")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket OPHELD4;
	@ObfuscatedName("cv")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket IF_BUTTOND;
	@ObfuscatedName("cs")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket OPNPC2;
	@ObfuscatedName("cm")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket MAP_BUILD_COMPLETE;
	@ObfuscatedName("ca")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket MESSAGE_PRIVATE;
	@ObfuscatedName("ci")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket RESUME_P_STRINGDIALOG;
	@ObfuscatedName("cx")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket IGNORELIST_DEL;
	@ObfuscatedName("cn")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket EVENT_CAMERA_POSITION;
	@ObfuscatedName("cc")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket OPNPC3;
	@ObfuscatedName("cy")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	static final ClientPacket UNUSED6;
	@ObfuscatedName("ct")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket OPLOC3;
	@ObfuscatedName("cd")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket SEND_SNAPSHOT;
	@ObfuscatedName("cw")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket WINDOW_STATUS;
	@ObfuscatedName("cf")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket CLICKWORLDMAP;
	@ObfuscatedName("cq")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket OPHELD_D;
	@ObfuscatedName("cg")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket IF_BUTTON10;
	@ObfuscatedName("cl")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket OPOBJ6;
	@ObfuscatedName("cu")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket OPPLAYER3;
	@ObfuscatedName("ch")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket OPNPC5;
	@ObfuscatedName("cz")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket REFLECTION_CHECK_REPLY;
	@ObfuscatedName("ce")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket OPPLAYER1;
	@ObfuscatedName("cj")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket OPLOC2;
	@ObfuscatedName("cb")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket OPHELDU;
	@ObfuscatedName("cp")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket EVENT_MOUSE_CLICK;
	@ObfuscatedName("dc")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket SET_CHATFILTERSETTINGS;
	@ObfuscatedName("dp")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket SEND_PING_REPLY;
	@ObfuscatedName("dg")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	public static final ClientPacket OPHELD2;
	@ObfuscatedName("dy")
	@ObfuscatedSignature(
		descriptor = "Ljj;"
	)
	static final ClientPacket UNUSED4;
	@ObfuscatedName("dh")
	@ObfuscatedGetter(
		intValue = -1090102203
	)
	@Export("id")
	final int id;
	@ObfuscatedName("dj")
	@ObfuscatedGetter(
		intValue = 1549322101
	)
	@Export("length")
	final int length;

	static {
		CLANCHANNEL_KICKUSER = new ClientPacket(0, -1); // L: 5
		EVENT_KEYBOARD = new ClientPacket(1, -2); // L: 6
		OPLOC6 = new ClientPacket(2, 2); // L: 7
		UNUSED1 = new ClientPacket(3, -1); // L: 8
		EVENT_MOUSE_MOVE = new ClientPacket(4, -1); // L: 9
		LOGIN_STATISTICS = new ClientPacket(5, -1); // L: 10
		UNKNOWN1 = new ClientPacket(6, 22); // L: 11
		EVENT_APPLET_FOCUS = new ClientPacket(7, 1); // L: 12
		IF_BUTTON4 = new ClientPacket(8, 8); // L: 13
		MOVE_GAMECLICK = new ClientPacket(9, -1); // L: 14
		AFFINEDCLANSETTINGS_ADDBANNED_FROMCHANNEL = new ClientPacket(10, -1); // L: 15
		IGNORELIST_ADD = new ClientPacket(11, -1); // L: 16
		IF_BUTTON5 = new ClientPacket(12, 8); // L: 17
		IF1_BUTTON4 = new ClientPacket(13, 8); // L: 18
		OPPLAYER4 = new ClientPacket(14, 3); // L: 19
		IF_BUTTONT = new ClientPacket(15, 16); // L: 20
		RESUME_P_NAMEDIALOG = new ClientPacket(16, -1); // L: 21
		IF_BUTTON2 = new ClientPacket(17, 8); // L: 22
		TELEPORT = new ClientPacket(18, 9); // L: 23
		UNUSED_2 = new ClientPacket(19, 7); // L: 24
		IF_BUTTON6 = new ClientPacket(20, 8); // L: 25
		UNUSED_3 = new ClientPacket(21, 2); // L: 26
		CLAN_KICKUSER = new ClientPacket(22, -1); // L: 27
		OPHELD1 = new ClientPacket(23, 8); // L: 28
		RESUME_P_COUNTDIALOG = new ClientPacket(24, 4); // L: 29
		MESSAGE_PUBLIC = new ClientPacket(25, -1); // L: 30
		BUG_REPORT = new ClientPacket(26, -2); // L: 31
		OPPLAYER2 = new ClientPacket(27, 3); // L: 32
		OPPLAYER6 = new ClientPacket(28, 3); // L: 33
		OPNPC4 = new ClientPacket(29, 3); // L: 34
		OPOBJ5 = new ClientPacket(30, 7); // L: 35
		OPHELD5 = new ClientPacket(31, 8); // L: 36
		OPPLAYER7 = new ClientPacket(32, 3); // L: 37
		OPLOCU = new ClientPacket(33, 15); // L: 38
		NO_TIMEOUT = new ClientPacket(34, 0); // L: 39
		OPPLAYER8 = new ClientPacket(35, 3); // L: 40
		OPHELD3 = new ClientPacket(36, 8); // L: 41
		OPNPCU = new ClientPacket(37, 11); // L: 42
		OPLOC1 = new ClientPacket(38, 7); // L: 43
		OPNPCT = new ClientPacket(39, 11); // L: 44
		CLIENT_CHEAT = new ClientPacket(40, -1); // L: 45
		IF_BUTTON8 = new ClientPacket(41, 8); // L: 46
		EXIT_FREECAM = new ClientPacket(42, 0); // L: 47
		FRIENDLIST_DEL = new ClientPacket(43, -1); // L: 48
		RESUME_PAUSEBUTTON = new ClientPacket(44, 6); // L: 49
		AFFINEDCLANSETTINGS_SETMUTED_FROMCHANNEL = new ClientPacket(45, -1); // L: 50
		OPLOC5 = new ClientPacket(46, 7); // L: 51
		OPPLAYER5 = new ClientPacket(47, 3); // L: 52
		IF1_BUTTON3 = new ClientPacket(48, 8); // L: 53
		OPOBJ3 = new ClientPacket(49, 7); // L: 54
		FRIENDLIST_ADD = new ClientPacket(50, -1); // L: 55
		OPLOC4 = new ClientPacket(51, 7); // L: 56
		OPPLAYERU = new ClientPacket(52, 11); // L: 57
		CLOSE_MODAL = new ClientPacket(53, 0); // L: 58
		IF_BUTTON7 = new ClientPacket(54, 8); // L: 59
		DETECT_MODIFIED_CLIENT = new ClientPacket(55, 4); // L: 60
		OPNPC1 = new ClientPacket(56, 3); // L: 61
		UNUSED5 = new ClientPacket(57, -1); // L: 62
		IF_BUTTON1 = new ClientPacket(58, 8); // L: 63
		IF1_BUTTON1 = new ClientPacket(59, 8); // L: 64
		IF1_BUTTON2 = new ClientPacket(60, 8); // L: 65
		OPOBJT = new ClientPacket(61, 15); // L: 66
		OPOBJU = new ClientPacket(62, 15); // L: 67
		IF_BUTTON3 = new ClientPacket(63, 8); // L: 68
		UPDATE_APPEARANCE = new ClientPacket(64, 13); // L: 69
		OPOBJ1 = new ClientPacket(65, 7); // L: 70
		BUTTON_CLICK = new ClientPacket(66, 4); // L: 71
		IF_BUTTON9 = new ClientPacket(67, 8); // L: 72
		CLAN_JOINCHAT_LEAVECHAT = new ClientPacket(68, -1); // L: 73
		OPNPC6 = new ClientPacket(69, 2); // L: 74
		OPOBJ4 = new ClientPacket(70, 7); // L: 75
		OPHELDT = new ClientPacket(71, 14); // L: 76
		RESUME_P_OBJDIALOG = new ClientPacket(72, 2); // L: 77
		OPOBJ2 = new ClientPacket(73, 7); // L: 78
		OPPLAYERT = new ClientPacket(74, 11); // L: 79
		MOVE_MINIMAPCLICK = new ClientPacket(75, -1); // L: 80
		OPLOCT = new ClientPacket(76, 15); // L: 81
		IF1_BUTTON5 = new ClientPacket(77, 8); // L: 82
		FRIEND_SETRANK = new ClientPacket(78, -1); // L: 83
		IDLE_LOGOUT = new ClientPacket(79, 0); // L: 84
		OPHELD4 = new ClientPacket(80, 8); // L: 85
		IF_BUTTOND = new ClientPacket(81, 16); // L: 86
		OPNPC2 = new ClientPacket(82, 3); // L: 87
		MAP_BUILD_COMPLETE = new ClientPacket(83, 0); // L: 88
		MESSAGE_PRIVATE = new ClientPacket(84, -2); // L: 89
		RESUME_P_STRINGDIALOG = new ClientPacket(85, -1); // L: 90
		IGNORELIST_DEL = new ClientPacket(86, -1); // L: 91
		EVENT_CAMERA_POSITION = new ClientPacket(87, 4); // L: 92
		OPNPC3 = new ClientPacket(88, 3); // L: 93
		UNUSED6 = new ClientPacket(89, -1); // L: 94
		OPLOC3 = new ClientPacket(90, 7); // L: 95
		SEND_SNAPSHOT = new ClientPacket(91, -1); // L: 96
		WINDOW_STATUS = new ClientPacket(92, 5); // L: 97
		CLICKWORLDMAP = new ClientPacket(93, 4); // L: 98
		OPHELD_D = new ClientPacket(94, 9); // L: 99
		IF_BUTTON10 = new ClientPacket(95, 8); // L: 100
		OPOBJ6 = new ClientPacket(96, 2); // L: 101
		OPPLAYER3 = new ClientPacket(97, 3); // L: 102
		OPNPC5 = new ClientPacket(98, 3); // L: 103
		REFLECTION_CHECK_REPLY = new ClientPacket(99, -1); // L: 104
		OPPLAYER1 = new ClientPacket(100, 3); // L: 105
		OPLOC2 = new ClientPacket(101, 7); // L: 106
		OPHELDU = new ClientPacket(102, 16); // L: 107
		EVENT_MOUSE_CLICK = new ClientPacket(103, 6); // L: 108
		SET_CHATFILTERSETTINGS = new ClientPacket(104, 3); // L: 109
		SEND_PING_REPLY = new ClientPacket(105, 10); // L: 110
		OPHELD2 = new ClientPacket(106, 8); // L: 111
		UNUSED4 = new ClientPacket(107, 6); // L: 112
	}

	ClientPacket(int var1, int var2) {
		this.id = var1; // L: 117
		this.length = var2; // L: 118
	} // L: 119
}
