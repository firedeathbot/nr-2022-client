import net.runelite.mapping.Export;
import net.runelite.mapping.ObfuscatedName;
import net.runelite.mapping.ObfuscatedSignature;

import java.math.BigInteger;

@ObfuscatedName("bt")
public class class67 {
	@ObfuscatedName("o")
	static final BigInteger field875;
	@ObfuscatedName("q")
	static final BigInteger field871;
	@ObfuscatedName("w")
	@ObfuscatedSignature(
		descriptor = "Lqd;"
	)
	@Export("options_buttons_0Sprite")
	static IndexedSprite options_buttons_0Sprite;

	static {
		field875 = new BigInteger("65537"); // L: 6
		field871 = new BigInteger(
				"9bd0220dd8ba90fb50d00115bf9616c936013e54d0271cb5109e1f39b9fc91483633e6bbe8aeb1bf4175520973b24adc60598eaa848bf28b0dba4ab5a2fc984c8c1a5e059e98170870ac78b6951bd5a090c503509d1f1d19e17a45edafbb632fe490cec99825a9093e2cdd6aa39fc6f095845ae722ff41e3ac1729f20cc9d92b491bf5d0da7ec3fe4ca12474a018696d4923fda322657209822b923f5698e209af0343a3e970360167365a0087260f9d9de24e62bf6a981fe4077cd03e44df7ff3b4b8afebb75c1ad139dca9771aba56b0a2a1c541772f5e692cf3d0b1be2f5855ddedb3c40b4daa89b2f137d1b262f1902dc497e72328683aaa626932efcff1",
				16);
	}

	@ObfuscatedName("p")
	@ObfuscatedSignature(
		descriptor = "(Lpx;IIIIIIB)V",
		garbageValue = "109"
	)
	@Export("loadTerrain")
	static final void loadTerrain(Buffer var0, int var1, int var2, int var3, int var4, int var5, int var6) {
		int var7;
		if (var2 >= 0 && var2 < 104 && var3 >= 0 && var3 < 104) { // L: 168
			Tiles.Tiles_renderFlags[var1][var2][var3] = 0; // L: 169

			while (true) {
				var7 = var0.readUnsignedByte(); // L: 171
				if (var7 == 0) { // L: 172
					if (var1 == 0) {
						Tiles.Tiles_heights[0][var2][var3] = -GrandExchangeOfferUnitPriceComparator.method5846(var4 + var2 + 932731, 556238 + var3 + var5) * 8; // L: 173
					} else {
						Tiles.Tiles_heights[var1][var2][var3] = Tiles.Tiles_heights[var1 - 1][var2][var3] - 240; // L: 174
					}
					break;
				}

				if (var7 == 1) { // L: 177
					int var8 = var0.readUnsignedByte(); // L: 178
					if (var8 == 1) { // L: 179
						var8 = 0;
					}

					if (var1 == 0) { // L: 180
						Tiles.Tiles_heights[0][var2][var3] = -var8 * 8;
					} else {
						Tiles.Tiles_heights[var1][var2][var3] = Tiles.Tiles_heights[var1 - 1][var2][var3] - var8 * 8; // L: 181
					}
					break;
				}

				if (var7 <= 49) { // L: 184
					Tiles.Tiles_overlays[var1][var2][var3] = var0.readByte(); // L: 185
					Tiles.Tiles_shapes[var1][var2][var3] = (byte)((var7 - 2) / 4); // L: 186
					RunException.field4812[var1][var2][var3] = (byte)(var7 - 2 + var6 & 3); // L: 187
				} else if (var7 <= 81) { // L: 190
					Tiles.Tiles_renderFlags[var1][var2][var3] = (byte)(var7 - 49); // L: 191
				} else {
					Tiles.Tiles_underlays[var1][var2][var3] = (byte)(var7 - 81); // L: 194
				}
			}
		} else {
			while (true) {
				var7 = var0.readUnsignedByte(); // L: 199
				if (var7 == 0) { // L: 200
					break;
				}

				if (var7 == 1) { // L: 201
					var0.readUnsignedByte(); // L: 202
					break;
				}

				if (var7 <= 49) { // L: 205
					var0.readUnsignedByte();
				}
			}
		}

	} // L: 208

	@ObfuscatedName("s")
	@ObfuscatedSignature(
		descriptor = "(Llp;IB)Lqd;",
		garbageValue = "69"
	)
	public static IndexedSprite method1883(AbstractArchive var0, int var1) {
		if (!class28.method374(var0, var1)) { // L: 130
			return null;
		} else {
			IndexedSprite var3 = new IndexedSprite(); // L: 133
			var3.width = class453.SpriteBuffer_spriteWidth; // L: 134
			var3.height = class453.SpriteBuffer_spriteHeight; // L: 135
			var3.xOffset = class453.SpriteBuffer_xOffsets[0]; // L: 136
			var3.yOffset = class453.SpriteBuffer_yOffsets[0]; // L: 137
			var3.subWidth = ScriptFrame.SpriteBuffer_spriteWidths[0]; // L: 138
			var3.subHeight = class453.SpriteBuffer_spriteHeights[0]; // L: 139
			var3.palette = Decimator.SpriteBuffer_spritePalette; // L: 140
			var3.pixels = class127.SpriteBuffer_pixels[0]; // L: 141
			VarcInt.method3325(); // L: 142
			return var3; // L: 145
		}
	}

	@ObfuscatedName("ki")
	@ObfuscatedSignature(
		descriptor = "(II)V",
		garbageValue = "1989518897"
	)
	static final void method1881(int var0) {
		var0 = Math.min(Math.max(var0, 0), 127); // L: 12073
		class12.clientPreferences.updateSoundEffectVolume(var0); // L: 12074
	} // L: 12075
}
