package net.runelite.mixins;

import net.runelite.api.Buffer;
import net.runelite.api.mixins.Copy;
import net.runelite.api.mixins.Mixin;
import net.runelite.api.mixins.Replace;
import net.runelite.api.mixins.Shadow;
import net.runelite.rs.api.RSClient;
import net.runelite.rs.api.RSWorldMapAreaData;

@Mixin(RSWorldMapAreaData.class)
public abstract class RSWorldMapAreaDataMix implements RSWorldMapAreaData
{
    @Shadow("client")
    private static RSClient client;

    @Copy("init")
    abstract void copy$init(Buffer var1, Buffer var2, int var3, boolean var4);

    @Replace("init")
    void init(Buffer var1, Buffer var2, int var3, boolean var4)
    {
        try {
            copy$init(var1, var2, var3, var4);
        } catch (Exception e){
            client.getLogger().error("Failed to init WrodlMapAreaData {} {} {} {}", var3, var4, var1, var2, e);
        }
    }
}
