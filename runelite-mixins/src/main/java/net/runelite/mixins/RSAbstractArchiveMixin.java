package net.runelite.mixins;

import com.google.common.hash.Hashing;
import com.google.common.io.ByteStreams;
import com.google.common.io.CharStreams;
import net.runelite.api.mixins.*;
import net.runelite.api.overlay.OverlayIndex;
import net.runelite.rs.api.RSAbstractArchive;
import net.runelite.rs.api.RSArchive;
import net.runelite.rs.api.RSClient;
import org.slf4j.Logger;

import java.io.*;
import java.util.Map;

@Mixin(RSAbstractArchive.class)
public abstract class RSAbstractArchiveMixin implements RSAbstractArchive
{
  @Shadow("client")
  private static RSClient client;

  @Shadow("customClientScripts")
  private static Map<Integer, byte[]> customClientScripts;

  @Inject
  private boolean overlayOutdated;

  @Override
  public boolean isOverlayOutdated()
  {
    return overlayOutdated;
  }

  @SuppressWarnings("InfiniteRecursion")
  @Copy("takeFile")
  @Replace("takeFile")
  public byte[] copy$getConfigData(int groupId, int fileId)
  {
    final int indexId = ((RSArchive) this).getIndex();
    String liveKey = liveDataKey(indexId, groupId, fileId);
    File liveFile = liveDataFile(liveKey);
    if (liveFile != null) {
      try {
        System.out.println("Using live data file: " + liveFile.getName());
        return ByteStreams.toByteArray(new FileInputStream(liveFile));
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

    final byte[] rsData = copy$getConfigData(groupId, fileId);
    if (!OverlayIndex.hasOverlay(indexId, groupId))
    {
      return rsData;
    }

    final Logger log = client.getLogger();
    final String path = String.format("/runelite/%s/%s", indexId, groupId);

    // rsData will be null if this script didn't exist at first
    if (rsData != null)
    {
      String overlayHash, originalHash;

      try (final InputStream hashIn = getClass().getResourceAsStream(path + ".hash"))
      {
        overlayHash = CharStreams.toString(new InputStreamReader(hashIn));
        originalHash = Hashing.sha256().hashBytes(rsData).toString();
      }
      catch (IOException e)
      {
        log.warn("Missing overlay hash for {}/{}", indexId, groupId);
        return rsData;
      }

      // Check if hash is correct first, so we don't have to load the overlay file if it doesn't match
      if (!overlayHash.equalsIgnoreCase(originalHash))
      {
        log.error("Script " + groupId + " is invalid, and will not be overlaid. This will break plugin(s)! Original hash: " + originalHash);
        overlayOutdated = true;
        return rsData;
      }
    }

    if (customClientScripts.containsKey(indexId << 16 | groupId))
    {
      return customClientScripts.get(indexId << 16 | groupId);
    }

    try (final InputStream ovlIn = getClass().getResourceAsStream(path))
    {
      return ByteStreams.toByteArray(ovlIn);
    }
    catch (IOException e)
    {
      log.warn("Missing overlay data for {}/{}", indexId, groupId);
      return rsData;
    }
  }
 
  

  @Inject
  private String liveDataKey(int indexId, int groupId, int fileID) {
    return indexId + "-" + groupId + "-" + fileID;
  }

  @Inject
  private File liveDataFile(String key) {
    final String liveDataEnabledProperty = System.getProperty("LIVE_DATA_ENABLED", "false");
    final boolean liveDataEnabled = Boolean.parseBoolean(liveDataEnabledProperty);
    if (!liveDataEnabled) {
      return null;
    }

    final File OPENOSRS_DIR = new File(System.getProperty("user.home"), ".osnr");
    final File LIVE_RELOAD_SCRIPTS_DIR = new File(OPENOSRS_DIR, "live_archives");
    final File file = new File(LIVE_RELOAD_SCRIPTS_DIR, key + ".bin");
    if (!file.exists()) {
      return null;
    }
    return file;
  }
}
