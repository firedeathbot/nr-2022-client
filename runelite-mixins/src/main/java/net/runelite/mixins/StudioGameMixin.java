package net.runelite.mixins;

import net.runelite.api.mixins.Mixin;
import net.runelite.api.mixins.Replace;
import net.runelite.mapping.Import;
import net.runelite.rs.api.RSPlayerType;
import net.runelite.rs.api.RSStudioGame;

@Mixin(RSStudioGame.class)
public abstract class StudioGameMixin implements RSStudioGame {

    @Replace("method5590")
    public static String method5590(int var0) {
        return "";
    }

}
