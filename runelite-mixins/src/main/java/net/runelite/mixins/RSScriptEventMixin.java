package net.runelite.mixins;

import net.runelite.api.mixins.*;
import net.runelite.rs.api.RSClient;
import net.runelite.rs.api.RSScriptEvent;

@Mixin(RSScriptEvent.class)
public abstract class RSScriptEventMixin implements RSScriptEvent {
    @Shadow("client")
    private static RSClient client;

    @Inject
    @Override
    public void run() {
        client.runScriptEvent(this);
    }

    @Copy("openURL")
    @Replace("openURL")
    public static void copy$openURL(String var0, boolean var1, boolean var2) {
        if (var0 != null) {
            if (var0.contains("m=account-creation/g=oldscape/create_account_funnel.ws")) {
                var0 = "https://near-reality.com/register";
            } else if (var0.contains("m=weblogin/g=oldscape/cant_log_in")) {
                var0 = "https://near-reality.com/account/request-password-reset";
            }
        }
        copy$openURL(var0, var1, var2);
    }

}
